# TC7200

This is summary and effort to port OpenWRT on the Technicolor TC7200.

The reason behind that is that my ISP uses cable modem to connect me into the
Internet.

# Dump ROOT password over LAN
```
wget -q -O - http://192.168.0.1/goform/system/GatewaySettings.bin | strings
```
The last two lines are the password: `admin:admin`

# Links
- [Hackaday project](https://hackaday.io/project/3441/logs)
- [Linux kernel Fork](https://github.com/jclehner/linux-technicolor-tc7200)
- [Technicolor hacking wiki](https://github.com/hack-technicolor/hack-technicolor/)
- [Polish OpenWrt forum](https://eko-one-pl.translate.goog/forum/viewtopic.php?id=16869&_x_tr_sl=pl&_x_tr_tl=cs&_x_tr_hl=cs&_x_tr_pto=sc)
- [Rooting TC7210](https://www.serializing.me/2018/06/03/rooting-the-technicolor-7210/)
- [TC7210 and TC7230 models](https://github.com/tch-opensrc/TC72XX_LxG1.7.1mp1_OpenSrc)
- [Device Wiki](https://deviwiki.com/wiki/Technicolor_TC7200_(Thomson))
- [Bootloader for bcm3384](https://github.com/Broadcom/aeolus)
